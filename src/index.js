import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Pets from './pets';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Pets />, document.getElementById('root'));
registerServiceWorker();
